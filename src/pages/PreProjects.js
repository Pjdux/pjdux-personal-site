import React from "react";
import PreProjectWrap from "../styled-containers/ContactWrapper";
import { Link } from "react-router-dom";
import PreProjectStyle from "../components/LinkButton";

class PreProjects extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      value: "",
      message: "Enter The Name Of The Famous JS Library Created By Facebook",
      buttonMessage: "- - - - - - - -",
    };
  }

  setValue = (e) => {
    e.preventDefault();

    this.setState({
      value: e.target.value,
    });
  };

  handleSubmit = (e) => {
    e.preventDefault();

    this.setState({
      message:
        this.state.value === "react.js" || this.state.value === "React.js"
          ? "You Shall Pass!"
          : `Nope, Lol, That's Not It...`,
    });
  };

  setButton = () => {
    this.setState({
      message: "Hint:,  It Ends With .JS",

      buttonMessage:
        this.state.message !== "react.js".toLowerCase()
          ? "- - - - - - -"
          : "TAKE ME TO MY PROJECTS!",
    });
  };

  render() {
    const { value, message, buttonMessage } = this.state;
    return (
      <PreProjectWrap
        style={{
          display: "flex",
          flexDirection: "column",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <h4
          style={{
            fontSize: "calc(13px + 1.5vw)",
            textAlign: "center",
            fontFamily: "Monoton, sans-serif",
            letterSpacing: "6px",
          }}
        >
          {message}
        </h4>
        <div>
          <form action="submit" onSubmit={this.handleSubmit}>
            <input
              style={{ padding: "10px", margin: "45px" }}
              type="text"
              onChange={this.setValue}
            />
          </form>
        </div>
        <div>
          {" "}
          {value.toLowerCase() === "react.js" ? (
            <PreProjectStyle style={{ backgroundColor: "#5dd104" }}>
              {" "}
              <Link
                style={{ color: "#fff", backgroundColor: "#5dd104" }}
                to="/Projects"
              >
                Take Me To Projects
              </Link>
            </PreProjectStyle>
          ) : (
            <PreProjectStyle onClick={this.setButton}>
              {buttonMessage}
            </PreProjectStyle>
          )}
        </div>
      </PreProjectWrap>
    );
  }
}

export default PreProjects;
