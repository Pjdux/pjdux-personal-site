"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _styledComponents = _interopRequireDefault(require("styled-components"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _templateObject() {
  var data = _taggedTemplateLiteral(["\nheight: 100%;\n\nmin-height: 500px;\nbackground-color: #c2d3da;\n\n\n\n\n.contact {\n  display: flex;\nflex-direction: column;\n  align-items: center;\n  justify-content: center;\n\n  margin: auto;\n\n\n}\n\n.contact-social {\ntext-align: center;\n  width: 95%;\n\npadding-top: 50px;\n  margin-top: 10px;\n\nmax-width: 650px;\n.home-link {\n\ntext-align: center;\nmargin-top: 5px;\nh6 {\n  padding: 10px;\n  color: #222;\n  &:hover {\n    color: #ffffff;\n    transition:  all 0.1s ease-in-out;\n  }\n}\n}\n\n}\n\n  .fa {\n  margin: 10px;\n    transition: all 0.2s ease-in-out;\n    color: #222;\n   font-size: calc(2em + 2.5vw);\n\n\n    &:hover {\n      color: #fff;\n\n    }\n  }\n\n\n}\n\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var ContactWrapper = _styledComponents["default"].div(_templateObject());

var _default = ContactWrapper;
exports["default"] = _default;