"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _styledComponents = _interopRequireDefault(require("styled-components"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  .profile-wrapper {\n    display: flex;\n    align-items: center;\n    justify-content: center;\n  }\n\n  .profile-pic {\n    height: 180px;\n    width: 180px;\n    border-radius: 50%;\n\n    border: 4px solid #fff;\n    margin-bottom: 20px;\n    box-shadow: 10px 9px 0.5px #2570a3;\n    animation-delay: 90s;\n    &:hover {\n      transition: all 0.5s ease-in-out;\n    }\n  }\n  height: 100%;\n\n  color: #fcfaea;\n  background: #3498db;\n\n  .home {\n    display: flex;\n\n    margin: 0 auto;\n    min-height: 100vh;\n\n    flex-direction: column;\n    justify-content: center;\n    align-items: center;\n    padding: 100px 0 80px 0;\n    font-size: 1.4em;\n\n    .hi-there {\n      font-size: 3em;\n\n      text-align: center;\n      margin: 0;\n      font-family: \"Pacifico\", sans-serif;\n      margin-bottom: 30px;\n      color: #fcfaea;\n    }\n\n    p {\n      text-align: center;\n      line-height: calc(1.1em + 0.5vw);\n      font-weight: 300;\n    }\n\n    .headline {\n      font-family: \"Roboto\", sans-serif;\n      font-size: calc(30px + 0.2vw);\n      text-align: center;\n      line-height: calc(1.1em + 0.5vw);\n      font-weight: 300;\n    }\n  }\n  .just-scroll {\n    margin-top: 0px;\n  }\n\n  .intro-fill {\n    padding-top: 85px;\n    .fa {\n      margin-left: 15px;\n      font-size: 2em;\n    }\n  }\n\n  .scroll-arrow {\n    color: #fff;\n  }\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var HomeWrap = _styledComponents["default"].div(_templateObject());

var _default = HomeWrap;
exports["default"] = _default;