"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _styledComponents = _interopRequireDefault(require("styled-components"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  position: relative;\nheight: 100%;\nmin-height: 100vh;\ntransition: all 0.5s ease-in-out;\n\n* {\nbox-sizing: border-box;\n}\n.tilted-top { \n  font-family: 'Titillium Web', sans-serif;\n  transform: rotate(-90deg);\n  margin: 35px auto;\n  font-size: 14px;\n  color: #222;\n  }\n  .tilted-top2 {\n\nfont-family: 'Titillium Web', sans-serif;\n    transform: rotate(-90deg);\n    position: fixed;\n    bottom: 50%;\n     left: 0px;\n    cursor: pointer;\n    transition-duration: 0.2s;\n    transition-timing-function: linear;\n    transition-delay: 0s;\n    display: flex;\n    flex-direction: column;\n    text-align: center;\n    align-items: center;\n    font-size: 18px;\ncolor: rgb(52, 52, 52);\n  }\n}\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var MainLayout = _styledComponents["default"].div(_templateObject());

var _default = MainLayout;
exports["default"] = _default;