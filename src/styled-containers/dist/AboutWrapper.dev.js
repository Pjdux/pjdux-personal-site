"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _styledComponents = _interopRequireDefault(require("styled-components"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n         background-color: #77c9d4 !important;\n           min-height: 100vh;\n         color: white;\n\n\n.story {\n\n\n  img {\n    width: 100%;\n  max-width: 180px;\n    height: 180px;\n  }\n\n}\n\n.feed {\n\n\n  margin: 0 auto;\n  display: grid;\n  grid-template-columns: repeat(3, 180px);\n  grid-auto-rows: 1fr;\n  grid-gap: 20px 20px;\n\n\n}\n  .about {\n\n\n        display: flex;\n        flex-direction: column;\n\n        padding: 100px 0 100px 0;\n        align-items: center;\n        min-height: 100vh;\n        color: #fff;\n        width: 90%;\n        max-width: 850px;\n        line-height: 1.5em;\n        font-family: 'Open Sans', sans-serif;\n\n         p  {\n          font-size: 0.9em;\n           letter-spacing: 0.1em;\n         }\n.celeb {\n  width: 95%;\n  max-width: 550px;\n}\n\n.bob {\ndisplay: inline-block;\nborder-radius: 50%;\npadding: 10px;\nbackground: #fff;\nmargin: 0 10px;\n  img{\n\n  width: 50px;\n  margin: 0 auto;\n\n  }\n}\n\n      font-weight: 100;\n      margin: 0 auto;\n      h1 {\n        text-align: center;\n        line-height: 1.5em;\n        font-family: 'Pacifico', sans-serif;\n            font-size: 2.5em;\n\n      }\n\n}\n\n.beatle {\n\nwidth: 125px;\nheight: 80px;\nimg {\n  width: 200px;\nheight: 100px;\n}\n\n}\n\n.who {\n\nletter-spacing: 0.2em;\n\n}\n\n}\n\n\n.forest-gif-container {\n\n  margin-top: 40px;\n\n\n  width: 450px;\n\n  height: 200px;\n\n\n\n.forest-gif {\nwidth: 100% !important;\nheight: 225px;\n\n}\n\n\n  @media only screen and (max-width: 500px){\n\n    align-self: center;\n    width: 70%;\n    max-width: 250px;\n    height: 200px;\n    max-height: 300px;\n\n\n\n  }\n\n\n\n}\n\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var AboutWrapper = _styledComponents["default"].div(_templateObject());

var _default = AboutWrapper;
exports["default"] = _default;