"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ResumeWrapper = void 0;

var _styledComponents = _interopRequireDefault(require("styled-components"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n\n  color: #222;\n  min-height: 100vh;\n\nbackground-color: #222;\ndisplay: flex;\n    flex-direction: column;\n    justify-content: center;\n    align-items: center;\n\n\n.resume {\n\n}\n\n  .resume-img {\n    width: 100%;\n    max-width: 900px;\n    margin: 0 auto;\n    display: flex;\n    flex-direction: column;\n    justify-content: center;\n    align-items: center;\n\n  }\n\n\n  @media only screen and (max-width: 500px) {\n\n\n\n  }\n\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var ResumeWrapper = _styledComponents["default"].div(_templateObject());

exports.ResumeWrapper = ResumeWrapper;