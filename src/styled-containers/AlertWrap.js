import styled from "styled-components";

const AlertWrap = styled.div`
  width: 20rem;
  background: #fff;
  padding: 1rem;
  border-radius: 3px;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

export default AlertWrap;
