import React, { useEffect } from "react";
import { useDispatch } from "react-redux";
import AlertWrap from "../styled-containers/AlertWrap";
import LinkButton from "./LinkButton";
const Alert = ({ text, buttonText, type, onClick }) => {
  const dispatch = useDispatch();

  useEffect(() => {
    if (!onClick) {
      const timer = setTimeout(() => {
        dispatch({ type });
      }, 6000);
      return () => clearTimeout(timer);
    }
  }, [dispatch, onClick, type]);

  return (
    <AlertWrap className="alert">
      {text}{" "}
      {buttonText && (
        <LinkButton primary onClick={onClick}>
          {buttonText}
        </LinkButton>
      )}
    </AlertWrap>
  );
};

export default Alert;
