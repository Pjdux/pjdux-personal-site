import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";

// Stateless component / Functional component
const Field = styled.div`
  input {
    max-width: 500px;
    padding: 20px;
    margin-bottom: 20px;
    font-family: "Allerta Stencil", sans-serif;
    font-weight: 100;

    outline: none;
    border: none;
    border-bottom: 1px solid rgb(235, 235, 235);
    background-color: transparent;
    font-size: 1em;

    color: #fff;
    &::placeholder {
      color: #fff;
    }
  }
`;

// PropTypes is a way to ensure we are expecting
// certain props that will enable the component to
// function properly.
Field.propTypes = {
  label: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  textarea: PropTypes.bool.isRequired,
  value: PropTypes.string.isRequired
};

Field.defaultProps = {
  textarea: false
};

export default props => (
  <Field className="field">
    <label>{props.label}</label>
    <input
      placeholder={props.placeholder}
      onChange={props.onChange}
      type={props.textarea ? "textarea" : "text"}
      value={props.value}
    />
  </Field>
);
