/* eslint-disable no-unused-vars */
import React from "react";
import styled from "styled-components";

const Overlay = styled.div`
  width: 180px;
  height: 180px;
  background-color: rgba(0, 0, 0, 0.15);
  border-radius: 50%;

  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;

const Overlay1 = Overlay.extend`
  width: 500px;
  height: 500px;
`;

export default Overlay;
