"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _styledComponents = _interopRequireDefault(require("styled-components"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  background: #f1f1f2;\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  align-items: center;\n  padding-top: 100px;\n\n  .familiar-with {\n    h6 {\n      font-family: \"Open Sans\", sans-serif;\n      color: #262228;\n\n      text-align: center;\n    }\n    ol li {\n      color: #1f92d2;\n      font-family: \"Open Sans\", sans-serif;\n    }\n  }\n\n  h2 {\n    font-family: \"Open Sans\", sans-serif;\n    color: #262228;\n    font-weight: 300;\n    align-self: center;\n    text-align: center;\n    padding: 30px;\n    width: 80%;\n    max-width: 450px;\n  }\n\n  h3 {\n    color: #222;\n    margin-top: 20px;\n    font-size: calc(12px + 0.2vw);\n    font-weight: 400;\n  }\n\n  .tech-list {\n    margin: auto;\n    display: grid;\n    grid-template-columns: repeat(4, 24%);\n    grid-template-rows: repeat(2, 40%);\n    grid-gap: 20px;\n\n    justify-content: center;\n    align-items: center;\n    height: 400px;\n    max-height: 500px;\n\n    color: #1c92d2;\n    @media only screen and (max-width: 737px) {\n      height: 250px;\n    }\n    .fa {\n      text-align: center;\n      font-size: calc(40px + 3.5vw);\n\n      width: 100%;\n\n      @media only screen and (min-width: 2000px) {\n        font-size: 135px;\n      }\n    }\n  }\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var Likes = _styledComponents["default"].div(_templateObject());

var _default = Likes;
exports["default"] = _default;