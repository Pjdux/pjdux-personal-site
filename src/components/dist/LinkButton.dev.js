"use strict";

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _styledComponents = _interopRequireWildcard(require("styled-components"));

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _templateObject2() {
  var data = _taggedTemplateLiteral(["\n      margin: 20px;\n    &:hover {\n      box-shadow:  0px 37px 20px -20px #333;\n  transform: translate(0px, -10px) scaleX(1.10);\n  font-size: 0.7em;\ncolor: #fff;\n\n  box-shadow: inset 0 0 2px 2px #333;\n\n"]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  font-family: \"Titillium Web\", sans-serif;\n  border-radius: 3px;\n\n  padding: 20px;\n  width: 180px;\n  font-size: 19px;\n\n  text-align: center;\n  background-color: transparent;\n  border: 2px solid #fff;\n\n  color: #f5f5f5;\n  margin: 50px;\n\n  width: 180px;\n\n  box-shadow: 0px 13px 10px -10px #333;\n\n  transition: all ease-in-out 175ms;\n  cursor: pointer;\n\n  &:hover {\n    box-shadow: 0px 37px 20px -20px #77b1ad;\n    transform: translate(0px, -10px) scaleX(1.1);\n    font-size: 0.9em;\n  }\n\n  ", ";\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var LinkButton = _styledComponents["default"].div(_templateObject(), function (props) {
  return props.primary && (0, _styledComponents.css)(_templateObject2());
});

var _default = LinkButton;
exports["default"] = _default;