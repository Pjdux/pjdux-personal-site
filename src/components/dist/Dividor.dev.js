"use strict";

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _styledComponents = _interopRequireWildcard(require("styled-components"));

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _templateObject6() {
  var data = _taggedTemplateLiteral(["\nmargin-top: 1px;\n\ntransform: skew(-20deg) rotate(-15deg);\nwidth: 16px;\nbackground-color: rgb(153, 153, 153);\n\n\n}\n\n\n"]);

  _templateObject6 = function _templateObject6() {
    return data;
  };

  return data;
}

function _templateObject5() {
  var data = _taggedTemplateLiteral(["\nmargin-top: 1px;\n\ntransform: skew(-20deg) rotate(-15deg);\nwidth: 21px;\nbackground-color: rgb(153, 153, 153);\n\n\n}\n\n\n"]);

  _templateObject5 = function _templateObject5() {
    return data;
  };

  return data;
}

function _templateObject4() {
  var data = _taggedTemplateLiteral(["\nmargin-top: 1px;\n\ntransform: skew(-20deg) rotate(-15deg);\nwidth: 21px;\n\n\n}\n\n"]);

  _templateObject4 = function _templateObject4() {
    return data;
  };

  return data;
}

function _templateObject3() {
  var data = _taggedTemplateLiteral(["\nbackground-color: rgb(153, 153, 153);\n\n}\n\n"]);

  _templateObject3 = function _templateObject3() {
    return data;
  };

  return data;
}

function _templateObject2() {
  var data = _taggedTemplateLiteral(["\nwidth: 16px;\nmargin-top: 1px;\n\n}\n\n\n"]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n\n\nbackground-color: #1c92d2;\n\nmargin-top: 15px;\n\nwidth: 24px;\nheight: 3px;\n\n-webkit-transition: .72s cubic-bezier(.52,.01,.16,1);\n-moz-transition: .72s cubic-bezier(.52,.01,.16,1);\ntransition: .72s cubic-bezier(.52,.01,.16,1);\n-webkit-transform: skewY(-16deg) scaleX(1);\n-moz-transform: skewY(-16deg) scaleX(1);\n-ms-transform: skewY(-16deg) scaleX(1);\n-o-transform: skewY(-16deg) scaleX(1);\ntransform: skewY(-16deg) scaleX(1);\n-webkit-transform-origin: 0 100%;\n-moz-transform-origin: 0 100%;\n-ms-transform-origin: 0 100%;\n-o-transform-origin: 0 100%;\ntransform-origin: 0 100%;\n\ntransform: skew(-20deg) rotate(-15deg);\n", "\n", "\n\n", "\n", "\n\n", "\n\n\n\n\n\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var Dividor = _styledComponents["default"].div(_templateObject(), function (props) {
  return props.primary && (0, _styledComponents.css)(_templateObject2());
}, function (props) {
  return props.primar && (0, _styledComponents.css)(_templateObject3());
}, function (props) {
  return props.pri && (0, _styledComponents.css)(_templateObject4());
}, function (props) {
  return props.primar2 && (0, _styledComponents.css)(_templateObject5());
}, function (props) {
  return props.primar3 && (0, _styledComponents.css)(_templateObject6());
});

var _default = Dividor;
exports["default"] = _default;