"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _styledComponents = _interopRequireDefault(require("styled-components"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n\n  width: 100%;\n  display: -webkit-flex;\n  display: flex;\n  padding: 50px 35px;\n  height: 0;\n  transition: all 0.2s ease-in-out;\n  overflow: visible;\n  \n\n  .logox {\n    img {\n      height: 200px;\n    }\n    &.active {\n      opacity: 0.5;\n      transition: all 2s ease-in-out;\n    }\n  }\n  &.open {\n    height: 100vh;\n    padding-top: 25%;\n    background-color: rgba(000, 000, 000, 0.9);\n\n    li:nth-child(2) {\n      display: block;\n    }\n\n    li:nth-child(3) {\n      display: block;\n    }\n  }\n\n  ul {\n    display: flex;\n    width: 100%;\n    justify-content: space-between;\n    list-style: none;\n    font-size: 0.9em;\n\n    img {\n      height: 125px;\n      width: 125px;\n      align-self: center;\n    }\n  }\n\n  li {\n    display: block;\n    font-family: \"Titillium Web\", sans-serif;\n\n    font-weight: 300;\n    color: #fff;\n\n    a {\n      color: #fff;\n    }\n    &.activex {\n      color: #fcfaea;\n      font-weight: bold;\n    }\n    &:hover {\n      font-weight: bold;\n      zoom: 1.01;\n      transition: all 180ms ease-in-out;\n    }\n\n    &:first-child {\n      margin-right: auto;\n      margin-top: -20px;\n      zoom: 1;\n      &:hover {\n        text-shadow: 0 0 50px #f2f2f2;\n      }\n    }\n\n    &:nth-child(3) {\n      margin-left: 15px;\n    }\n\n    &:nth-child(4) {\n      display: none;\n    }\n    &:nth-child(5) {\n      margin-left: 15px;\n    }\n\n  }\n  // media queries//\n\n  @media only screen and (max-width: 550px) {\n    margin: 0;\n    padding: 0;\n    overflow: hidden;\n\n    ul {\n      display: flex;\n\n      flex-direction: column;\n      height: 100%;\n    }\n    li {\n      font-size: 1.5em;\n      margin-bottom: 15px;\n      width: 100%;\n      text-align: center;\n\n\n\n\n      a {\n        color: #fff;\n        font-family: \"Titillium Web\", sans-serif;\n        font-weight: 400;\n        text-transform: uppercase;\n      }\n\n      &:nth-child(3) {\n        margin-left: 0;\n        zoom: 1;\n        &:hover {\n          text-shadow: 0 0 50px #f2f2f2;\n        }\n      }\n      &:nth-child(2) {\n        zoom: 1;\n        &:hover {\n          text-shadow: 0 0 50px #f2f2f2;\n        }\n      }\n      &:last-child {\n        zoom: 1;\n      }\n\n      &:nth-child(4) {\n        display: block;\n        font-size: 12px;\n        zoom: 2;\n\n      }\n\n      &:nth-child(5) {\n\n\nmargin: 0 auto;\n\n      }\n    }\n  }\n  //end of query\n\n  .social-nav {\n    height: 100px;\n    width: 200px;\n    padding: 10px;\n    margin: 0 auto;\n    display: flex;\n    justify-content: space-between;\n    align-items: center;\n    &:hover {\n      background: transparent;\n    }\n\n    .fa {\n      transition: all 0.1s ease-in-out;\n      color: #8c8c8c;\n      &:hover {\n        color: #f1f1f1;\n      }\n    }\n    @media only screen and (min-width: 527px) {\n      display: none;\n    }\n  }\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var NavWrap = _styledComponents["default"].nav(_templateObject());

var _default = NavWrap;
exports["default"] = _default;