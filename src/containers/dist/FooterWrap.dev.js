"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _styledComponents = _interopRequireDefault(require("styled-components"));

require("animate.css/animate.min.css");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  background-color: #f1f5f5;\n  position: relative;\n  height: 100%;\n  min-height: 500px;\n\n  display: flex;\n  justify-content: center;\n\n  align-items: center;\n  flex-direction: column;\n\n  .shy {\n    font-family: \"Molengo\", sans-serif;\n    font-weight: 300;\n    font-size: calc(20px + 0.1vw);\n    letter-spacing: 1px;\n    color: #444;\n    text-transform: uppercase;\n    &::after {\n      content: \" !\";\n      color: #2992d2;\n    }\n  }\n\n  .foot-dividor {\n    padding-bottom: 60px;\n    display: flex;\n    flex-direction: column;\n    align-items: center;\n    justify-content: center;\n    color: #2992d2;\n  }\n\n  .peace {\n    transform: rotate(25deg);\n    color: #fff;\n    font-size: 7em;\n    filter: drop-shadow(-9px 10px 1px #2992d2);\n  }\n  .say-hello {\n    font-family: \"Monoton\";\n    margin: 0 auto;\n    font-size: calc(2.3em + 2.5vw);\n    color: #fff;\n    filter: drop-shadow(10px 8px 0px #2992d2);\n    margin-bottom: 100px;\n    text-align: center;\n  }\n\n  .footer-info {\n    position: absolute;\n    bottom: 0px;\n\n    display: flex;\n    background-color: #ffffff;\n    color: #4f4f4f;\n\n    padding: 20px;\n    width: 100%;\n\n    font-family: \"Lato\", sans-serif;\n\n    h6 {\n      flex: 1;\n      font-size: 0.7em;\n    }\n    .social-footer {\n      display: flex;\n      justify-content: space-around;\n      width: 150px;\n      a {\n        transition: all 150ms ease-in-out;\n        color: #4f4f4f;\n        &:hover {\n          color: #3498db;\n        }\n      }\n    }\n    @media only screen and (max-width: 500px) {\n      justify-content: center;\n      align-items: center;\n      flex-direction: column;\n      margin: 0px auto;\n    }\n  }\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var FooterWrap = _styledComponents["default"].div(_templateObject());

var _default = FooterWrap;
exports["default"] = _default;