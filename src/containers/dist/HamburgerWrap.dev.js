"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _styledComponents = _interopRequireDefault(require("styled-components"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  .line-wrap {\n    width: 30px;\n    height: 45px;\n    position: fixed;\n    top: 50px;\n    right: 35px;\n    z-index: 10;\n  }\n\n  width: 100%;\n\n  position: fixed;\n  height: 0;\n  top: 0;\n  right: 0;\n  z-index: 1;\n  -webkit-transform: rotate(0deg);\n  -moz-transform: rotate(0deg);\n  -o-transform: rotate(0deg);\n  transform: rotate(0deg);\n  -webkit-transition: 0.5s ease-in-out;\n  -moz-transition: 0.5s ease-in-out;\n  -o-transition: 0.5s ease-in-out;\n  transition: 0.5s ease-in-out;\n  cursor: pointer;\n\n  .line {\n    position: absolute;\n    display: block;\n\n    height: 3px;\n    width: 100%;\n    background: #ffffff;\n    border-radius: 9px;\n    opacity: 1;\n    left: 0;\n    box-shadow: -1px 1px 2px #222;\n    -webkit-transform: rotate(0deg);\n    -moz-transform: rotate(0deg);\n    -o-transform: rotate(0deg);\n    transform: rotate(0deg);\n    -webkit-transition: 0.25s ease-in-out;\n    -moz-transition: 0.25s ease-in-out;\n    -o-transition: 0.25s ease-in-out;\n    transition: 0.25s ease-in-out;\n    z-index: 50;\n    &:nth-child(1) {\n      top: 0px;\n    }\n\n    &:nth-child(2),\n    &:nth-child(3) {\n      top: 9px;\n    }\n    &:nth-child(4) {\n      top: 18px;\n    }\n  }\n\n  &.menuActive .line:nth-child(1) {\n    top: 9px;\n    width: 0%;\n    left: 50%;\n  }\n\n  &.menuActive .line:nth-child(2) {\n    -webkit-transform: rotate(45deg);\n    -moz-transform: rotate(45deg);\n    -o-transform: rotate(45deg);\n    transform: rotate(45deg);\n  }\n\n  &.menuActive .line:nth-child(3) {\n    -webkit-transform: rotate(-45deg);\n    -moz-transform: rotate(-45deg);\n    -o-transform: rotate(-45deg);\n    transform: rotate(-45deg);\n  }\n\n  &.menuActive .line:nth-child(4) {\n    top: 18px;\n    width: 0%;\n    left: 50%;\n  }\n\n  @media only screen and (min-width: 550px) {\n    display: none;\n  }\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var HamburgerWrap = _styledComponents["default"].div(_templateObject());

var _default = HamburgerWrap;
exports["default"] = _default;