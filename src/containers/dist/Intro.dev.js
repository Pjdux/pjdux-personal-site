"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _styledComponents = _interopRequireDefault(require("styled-components"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  display: flex;\n  flex-direction: column;\n  align-items: center;\n  justify-content: center;\n\n  background: #3498db;\n  background: -webkit-linear-gradient(to bottom, #3498db, #41b3ff);\n  background: linear-gradient(to bottom, #3498db, #41b3ff);\n\n  h6 {\n    margin: 0 auto;\n  }\n  p {\n    transition: all 0.3s ease-in-out;\n\n    font-family: \"Titillium Web\", sans-serif;\n    letter-spacing: 0.03em;\n    font-size: calc(18px + 2.5vw);\n    line-height: calc(1.1em + 0.5vh);\n    font-weight: 300;\n    width: 95%;\n    max-width: 900px;\n\n    text-align: center;\n    color: #262228;\n    margin: 0px auto 75px auto;\n\n    .black {\n      color: #fff;\n    }\n    .last {\n      color: #fff;\n      text-shadow: 0 0 20px #fff;\n    }\n  }\n\n  .tech-bottom {\n    font-size: calc(16px + 0.5vw);\n    width: 69%;\n    max-width: 400px;\n  }\n  .learn-more {\n    color: #fff;\n    padding: 25px;\n    font-family: \"Titillium Web\", sans-serif;\n\n    margin: 50px;\n    &:hover {\n    }\n  }\n  .gif-wrap {\n    display: flex;\n    overflow: hidden;\n    width: 275px;\n    border-radius: 50%;\n    border: 4px solid #fff;\n    height: 275px;\n    background-color: #111;\n\n    .gif-Alive {\n      min-width: 100%;\n      min-height: 100%;\n    }\n\n    video {\n    }\n    .IdeaGif {\n      flex: 1;\n    }\n\n    .about-link {\n      display: flex;\n    }\n  }\n  @media only screen and (max-width: 550px) {\n    .gif-wrap {\n      overflow: hidden;\n    }\n  }\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var Intro = _styledComponents["default"].div(_templateObject());

var _default = Intro;
exports["default"] = _default;